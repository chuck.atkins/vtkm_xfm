
#ifndef vtkmxfm_h
#define vtkmxfm_h

#define VTKmXFM_VERSION_MAJOR 0
#define VTKmXFM_VERSION_MINOR 1
#define VTKM_VERSION_PATCH 0

#include "vtkmxfm/vtkm_xfm_export.h"
#include "vtkmxfm/ArrayHandleXGCPointCoordinates.h"
#include "vtkmxfm/CellSetToroid.h"
#include "vtkmxfm/XFMPolicy.h"


#ifdef VTKM_CUDA
  // Thee versions of these classes that are templated over the CUDA device are
  // not compiled into the library, but instead need to be instantiated in each
  // user.
  // If we moved to require separable compilation, and sm only flags we could
  // make CUDA templated classes be compiled into the vtkm_xfm library
  #include "vtkmxfm/internal/ConnectivityToroid.hxx"
  #include "vtkmxfm/internal/PointsXGC.hxx"
#endif

#endif

