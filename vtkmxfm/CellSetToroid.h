#ifndef vtkmxfm_CellSetToroid_h
#define vtkmxfm_CellSetToroid_h

#include "vtkmxfm/vtkm_xfm_export.h"

#include <vtkm/TopologyElementTag.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/CellSet.h>

#include "vtkmxfm/ArrayHandleXGCPointCoordinates.h"
#include "vtkmxfm/internal/ConnectivityToroid.h"
#include "vtkmxfm/internal/ThreadIndicesToroid.h"

namespace vtkm
{
namespace cont
{

class VTKM_XFM_EXPORT CellSetToroid : public CellSet
{
public:
  CellSetToroid();

  CellSetToroid(const vtkm::cont::ArrayHandle<vtkm::Int32>& conn,
                  vtkm::Int32 numPointsPerPlane,
                  vtkm::Int32 numPlanes,
                  bool periodic = true);

  ~CellSetToroid();

  CellSetToroid& operator=(const CellSetToroid& src);

  vtkm::Int32 GetNumberOfPlanes() const;

  vtkm::Id GetNumberOfCells() const override;

  vtkm::Id GetNumberOfPoints() const override;

  vtkm::Id GetNumberOfFaces() const override;

  vtkm::Id GetNumberOfEdges() const override;

  vtkm::Id2 GetSchedulingRange(vtkm::TopologyElementTagCell) const;

  vtkm::Id2 GetSchedulingRange(vtkm::TopologyElementTagPoint) const;

  void PrintSummary(std::ostream& out) const override;

  template <typename DeviceAdapter, typename FromTopology, typename ToTopology>
  struct ExecutionTypes;

  template <typename DeviceAdapter>
  struct ExecutionTypes<DeviceAdapter, vtkm::TopologyElementTagPoint, vtkm::TopologyElementTagCell>
  {
    using ExecObjectType = vtkm::exec::ConnectivityToroid<DeviceAdapter>;
  };

  template <typename DeviceAdapter>
  struct ExecutionTypes<DeviceAdapter, vtkm::TopologyElementTagCell, vtkm::TopologyElementTagPoint>
  {
    using ExecObjectType = vtkm::exec::ReverseConnectivityToroid<DeviceAdapter>;
  };

  template <typename Device>
  typename vtkm::exec::ConnectivityToroid<Device> PrepareForInput(Device,
                                                               vtkm::TopologyElementTagPoint,
                                                               vtkm::TopologyElementTagCell) const;

  template <typename Device>
  typename vtkm::exec::ReverseConnectivityToroid<Device>
    PrepareForInput(Device, vtkm::TopologyElementTagCell, vtkm::TopologyElementTagPoint) const;

private:
  vtkm::Int32 NumberOfCellsPerPlane;
  vtkm::Int32 NumberOfPointsPerPlane;
  vtkm::Int32 NumberOfPlanes;
  vtkm::cont::ArrayHandle<vtkm::Int32> Connectivity;

  bool PeriodicBoundary;

  mutable bool ReverseConnectivityBuilt;
  mutable vtkm::cont::ArrayHandle<vtkm::Int32> RConn;
  mutable vtkm::cont::ArrayHandle<vtkm::Int32> RNumIndices;
  mutable vtkm::cont::ArrayHandle<vtkm::Int32> RIndexOffsets;
};


template <typename T>
CellSetToroid make_CellSetToroid(const vtkm::cont::ArrayHandle<vtkm::Int32>& conn,
                                 const vtkm::cont::ArrayHandleXGCPointCoordinates<T>& coords,
                                 bool periodic = true)
{
  return CellSetToroid{ conn, coords.GetNumberOfPointsPerPlane(), coords.GetNumberOfPlanes(), periodic };
}

template <typename T>
CellSetToroid make_CellSetToroid(const int* conn,
                                 vtkm::Id length,
                                 const vtkm::cont::ArrayHandleXGCPointCoordinates<T>& coords,
                                 bool periodic = true)
{
  return make_CellSetToroid(make_ArrayHandle(conn, length), coords, periodic);
}

template <typename T>
CellSetToroid make_CellSetToroid(const std::vector<int>& conn,
                                 const vtkm::cont::ArrayHandleXGCPointCoordinates<T>& coords,
                                 bool periodic = true)
{
  if (!conn.empty())
  {
    return make_CellSetToroid(&conn.front(), static_cast<vtkm::Id>(conn.size()), coords, periodic);
  }
  else
  {
    return make_CellSetToroid(vtkm::cont::ArrayHandle<int>{}, coords, periodic);
  }
}
}
} // namespace vtkm::cont

#include <vtkm/cont/serial/DeviceAdapterSerial.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>
namespace vtkm
{
namespace cont
{
extern template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ConnectivityToroid<DeviceAdapterTagSerial>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagSerial,
                                   vtkm::TopologyElementTagPoint,
                                   vtkm::TopologyElementTagCell) const;
extern template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ReverseConnectivityToroid<DeviceAdapterTagSerial>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagSerial,
                                   vtkm::TopologyElementTagCell,
                                   vtkm::TopologyElementTagPoint) const;


#ifdef VTKM_ENABLE_TBB
extern template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ConnectivityToroid<DeviceAdapterTagTBB>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagTBB,
                                   vtkm::TopologyElementTagPoint,
                                   vtkm::TopologyElementTagCell) const;
extern template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ReverseConnectivityToroid<DeviceAdapterTagTBB>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagTBB,
                                   vtkm::TopologyElementTagCell,
                                   vtkm::TopologyElementTagPoint) const;
#endif
}
}
#endif
