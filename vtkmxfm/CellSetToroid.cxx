
#include "CellSetToroid.h"

namespace vtkm
{
namespace cont
{
CellSetToroid::CellSetToroid()
  : vtkm::cont::CellSet(std::string("xgc"))
  , NumberOfCellsPerPlane(0)
  , NumberOfPointsPerPlane(0)
  , NumberOfPlanes(0)
  , Connectivity()
  , PeriodicBoundary(true)
  , ReverseConnectivityBuilt(false)
  , RConn()
  , RNumIndices()
  , RIndexOffsets()
{
}

CellSetToroid::CellSetToroid(const vtkm::cont::ArrayHandle<vtkm::Int32>& conn,
                             vtkm::Int32 numPointsPerPlane,
                             vtkm::Int32 numPlanes,
                             bool periodic)
  : vtkm::cont::CellSet(std::string("xgc"))
  , NumberOfCellsPerPlane(static_cast<vtkm::Int32>(conn.GetNumberOfValues() / 3))
  , NumberOfPointsPerPlane(numPointsPerPlane)
  , NumberOfPlanes(numPlanes)
  , Connectivity(conn)
  , PeriodicBoundary(periodic)
  , ReverseConnectivityBuilt(false)
  , RConn()
  , RNumIndices()
  , RIndexOffsets()
{
}

CellSetToroid::~CellSetToroid() = default;

CellSetToroid& CellSetToroid::operator=(const CellSetToroid& src)
{
  this->CellSet::operator=(src);
  this->NumberOfCellsPerPlane = src.NumberOfCellsPerPlane;
  this->NumberOfPointsPerPlane = src.NumberOfPointsPerPlane;
  this->NumberOfPlanes = src.NumberOfPlanes;
  this->Connectivity = src.Connectivity;
  this->ReverseConnectivityBuilt = src.ReverseConnectivityBuilt;
  this->RConn = src.RConn;
  this->RNumIndices = src.RNumIndices;
  this->RIndexOffsets = src.RIndexOffsets;
  return *this;
}

vtkm::Int32 CellSetToroid::GetNumberOfPlanes() const
{
  return this->NumberOfPlanes;
}

vtkm::Id CellSetToroid::GetNumberOfCells() const
{
  if (this->PeriodicBoundary)
  {
    return static_cast<vtkm::Id>(this->NumberOfPlanes) *
      static_cast<vtkm::Id>(this->NumberOfCellsPerPlane);
  }
  else
  {
    return static_cast<vtkm::Id>(this->NumberOfPlanes - 1) *
      static_cast<vtkm::Id>(this->NumberOfCellsPerPlane);
  }
}

vtkm::Id CellSetToroid::GetNumberOfPoints() const
{
  return static_cast<vtkm::Id>(this->NumberOfPlanes) *
    static_cast<vtkm::Id>(this->NumberOfPointsPerPlane);
}

vtkm::Id CellSetToroid::GetNumberOfFaces() const
{
  return -1;
}

vtkm::Id CellSetToroid::GetNumberOfEdges() const
{
  return -1;
}

vtkm::Id2 CellSetToroid::GetSchedulingRange(vtkm::TopologyElementTagCell) const
{
  if (this->PeriodicBoundary)
  {
    return vtkm::Id2(this->NumberOfCellsPerPlane, this->NumberOfPlanes);
  }
  else
  {
    return vtkm::Id2(this->NumberOfCellsPerPlane, this->NumberOfPlanes - 1);
  }
}

vtkm::Id2 CellSetToroid::GetSchedulingRange(vtkm::TopologyElementTagPoint) const
{
  return vtkm::Id2(this->NumberOfPointsPerPlane, this->NumberOfPlanes);
}

void CellSetToroid::PrintSummary(std::ostream& out) const
{
  out << "   vtkmCellSetSingleType: " << this->Name << std::endl;
  out << "   NumberOfCellsPerPlane: " << this->NumberOfCellsPerPlane << std::endl;
  out << "   NumberOfPointsPerPlane: " << this->NumberOfPointsPerPlane << std::endl;
  out << "   NumberOfPlanes: " << this->NumberOfPlanes << std::endl;
  out << "   Connectivity: " << std::endl;
  vtkm::cont::printSummary_ArrayHandle(this->Connectivity, out);
  out << "   ReverseConnectivityBuilt: " << this->NumberOfPlanes << std::endl;
}

}
}
