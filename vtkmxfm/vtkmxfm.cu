

#include "vtkmxfm/vtkm_xfm_export.h"
#include "vtkmxfm/ArrayHandleXGCPointCoordinates.h"
#include "vtkmxfm/CellSetToroid.hxx"
#include "vtkmxfm/internal/ConnectivityToroid.hxx"
#include "vtkmxfm/internal/PointsXGC.hxx"

#include <vtkm/cont/cuda/DeviceAdapterCUDA.h>


//exports for the ArrayHandle<T, StorageTagPointsXGC> internal implementation
namespace vtkm{ namespace cont { namespace internal {

template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagCuda>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagCuda>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagCuda>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagCuda>;

}
}
}

//exports for the ConnectivityToroid
namespace vtkm { namespace cont {
template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ConnectivityToroid<DeviceAdapterTagCuda>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagCuda,
                              vtkm::TopologyElementTagPoint,
                              vtkm::TopologyElementTagCell) const;
template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ReverseConnectivityToroid<DeviceAdapterTagCuda>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagCuda,
                              vtkm::TopologyElementTagCell,
                              vtkm::TopologyElementTagPoint) const;
}
}
