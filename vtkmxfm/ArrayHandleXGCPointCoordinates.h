
#ifndef vtkmxfm_ArrayHandleXGCPointCoordinates_h
#define vtkmxfm_ArrayHandleXGCPointCoordinates_h

#include "vtkmxfm/vtkm_xfm_export.h"

#include "vtkmxfm/internal/PointsXGC.h"
#include <vtkm/cont/ArrayHandle.h>

namespace vtkm
{
namespace cont
{

template <typename T>
class VTKM_ALWAYS_EXPORT ArrayHandleXGCPointCoordinates
  : public vtkm::cont::ArrayHandle<vtkm::Vec<T, 3>, internal::StorageTagPointsXGC>
{

  using StorageType = vtkm::cont::internal::Storage<vtkm::Vec<T, 3>, internal::StorageTagPointsXGC>;

public:
  VTKM_ARRAY_HANDLE_SUBCLASS(
    ArrayHandleXGCPointCoordinates,
    (ArrayHandleXGCPointCoordinates<T>),
    (vtkm::cont::ArrayHandle<vtkm::Vec<T, 3>, internal::StorageTagPointsXGC>));

  ArrayHandleXGCPointCoordinates(const StorageType& storage)
    : Superclass(storage)
  {
  }

  vtkm::Int32 GetNumberOfPointsPerPlane() const { return (this->GetStorage().Length/2); }
  vtkm::Int32 GetNumberOfPlanes() const { return this->GetStorage().NumberOfPlanes; }
};

template <typename T>
vtkm::cont::ArrayHandleXGCPointCoordinates<T>
make_ArrayHandleXGCPointCoordinates(const T* array, vtkm::Id length, vtkm::Int32 numberOfPlanes, bool cylindrical = false)
{
  using StorageType = vtkm::cont::internal::Storage<vtkm::Vec<T, 3>, internal::StorageTagPointsXGC>;
  return ArrayHandleXGCPointCoordinates<T>(StorageType(array, length, numberOfPlanes, cylindrical));
}

template <typename T>
vtkm::cont::ArrayHandleXGCPointCoordinates<T> make_ArrayHandleXGCPointCoordinates(
  const std::vector<T>& array,
  vtkm::Int32 numberOfPlanes)
{
  if (!array.empty())
  {
    return make_ArrayHandleXGCPointCoordinates(
      &array.front(), static_cast<vtkm::Id>(array.size()), numberOfPlanes);
  }
  else
  {
    // Vector empty. Just return an empty array handle.
    return ArrayHandleXGCPointCoordinates<T>();
  }
}

}
}

// template classes we want to compile only once
namespace vtkm { namespace cont {
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleXGCPointCoordinates<float>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleXGCPointCoordinates<double>;
} }
#endif
