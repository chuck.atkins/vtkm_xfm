

#include "vtkmxfm/vtkm_xfm_export.h"
#include "vtkmxfm/ArrayHandleXGCPointCoordinates.h"
#include "vtkmxfm/CellSetToroid.hxx"
#include "vtkmxfm/internal/ConnectivityToroid.hxx"
#include "vtkmxfm/internal/PointsXGC.hxx"

#include <vtkm/cont/serial/DeviceAdapterSerial.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>

//exports for the ArrayPortalPointsXGC and Connectivity
namespace vtkm
{
namespace exec
{
template struct VTKM_XFM_TEMPLATE_EXPORT
  ArrayPortalPointsXGC<cont::internal::ArrayPortalFromIterators<float const*, void>>;
template struct VTKM_XFM_TEMPLATE_EXPORT
  ArrayPortalPointsXGC<cont::internal::ArrayPortalFromIterators<double const*, void>>;

template struct VTKM_XFM_TEMPLATE_EXPORT ConnectivityToroid<vtkm::cont::DeviceAdapterTagSerial>;
template struct VTKM_XFM_TEMPLATE_EXPORT ReverseConnectivityToroid<vtkm::cont::DeviceAdapterTagSerial>;

#ifdef VTKM_ENABLE_TBB
template struct VTKM_XFM_TEMPLATE_EXPORT ConnectivityToroid<vtkm::cont::DeviceAdapterTagTBB>;
template struct VTKM_XFM_TEMPLATE_EXPORT ReverseConnectivityToroid<vtkm::cont::DeviceAdapterTagTBB>;
#endif

}
}

//exports for the ArrayHandleXGCPointCoordinates
namespace vtkm { namespace cont {
template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleXGCPointCoordinates<float>;
template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleXGCPointCoordinates<double>;
} }

//exports for the ArrayHandle<T, StorageTagPointsXGC> internal implementation
namespace vtkm{ namespace cont { namespace internal {
template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<float, 3>, StorageTagPointsXGC>;
template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<double, 3>, StorageTagPointsXGC>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManagerBase<vtkm::Vec<float, 3>, StorageTagPointsXGC>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManagerBase<vtkm::Vec<double, 3>, StorageTagPointsXGC>;

template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;

#ifdef VTKM_ENABLE_TBB
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
#endif
}
}
}

//exports for the ConnectivityToroid
namespace vtkm { namespace cont {
template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ConnectivityToroid<DeviceAdapterTagSerial>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagSerial,
                              vtkm::TopologyElementTagPoint,
                              vtkm::TopologyElementTagCell) const;
template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ReverseConnectivityToroid<DeviceAdapterTagSerial>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagSerial,
                              vtkm::TopologyElementTagCell,
                              vtkm::TopologyElementTagPoint) const;

#ifdef VTKM_ENABLE_TBB
template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ConnectivityToroid<DeviceAdapterTagTBB>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagTBB,
                              vtkm::TopologyElementTagPoint,
                              vtkm::TopologyElementTagCell) const;
template VTKM_XFM_TEMPLATE_EXPORT vtkm::exec::ReverseConnectivityToroid<DeviceAdapterTagTBB>
  CellSetToroid::PrepareForInput(vtkm::cont::DeviceAdapterTagTBB,
                              vtkm::TopologyElementTagCell,
                              vtkm::TopologyElementTagPoint) const;
#endif
}
}
