
#ifndef vtkmxfm_internal_ToroidIndices_h
#define vtkmxfm_internal_ToroidIndices_h

#include <vtkm/CellShape.h>
#include <vtkm/TopologyElementTag.h>
#include <vtkm/VecFromPortal.h>
#include <vtkm/cont/ArrayHandle.h>

#include <vtkm/exec/arg/ThreadIndicesTopologyMap.h>

namespace vtkm
{
namespace exec
{

struct ToroidIndices
{
  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ToroidIndices() = default;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ToroidIndices(vtkm::Int32 pointsPerPlane,
                vtkm::Id plane,
                vtkm::Id nextPlane,
                const vtkm::Id3& pointIds)
    : PointsPerPlane(pointsPerPlane)
    , Plane(plane)
    , NPlane(nextPlane)
    , PointIds(pointIds)
  {
  }

  VTKM_EXEC
  vtkm::Id operator[](vtkm::IdComponent index) const
  {
    VTKM_ASSERT(index >= 0 && index < 6);
    if (index < 3)
    {
      return this->PointIds[index] + this->Plane * this->PointsPerPlane;
    }
    else
    {
      return this->PointIds[index-3] + this->NPlane * this->PointsPerPlane;
    }
  }

  vtkm::Id GetNumberOfComponents() const { return 6; }

  vtkm::Int32 PointsPerPlane;
  vtkm::Int32 Plane;
  vtkm::Int32 NPlane;
  vtkm::Id3 PointIds;
};

template <typename PortalType>
struct ReverseToroidIndices
{
  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ReverseToroidIndices() = default;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ReverseToroidIndices(const PortalType& portal,
                       vtkm::Int32 length,
                       vtkm::Int32 offset,
                       vtkm::Int32 cellsPerPlane,
                       vtkm::Id plane,
                       vtkm::Id nextPlane,
                       bool periodic = true)
    : PlanePortal(portal)
    , PlaneNumComponents(length)
    , PlaneOffset(offset)
    , CellsPerPlane(cellsPerPlane)
    , Plane(plane)
    , NPlane(nextPlane)
    , PeriodicBoundary(periodic)
  {
  }

  VTKM_EXEC
  vtkm::Id operator[](vtkm::IdComponent index) const
  {
    VTKM_ASSERT(index >= 0 && index < (this->PlaneNumComponents * 2));

    if (!this->PeriodicBoundary && this->Plane == 0)
    {
      //we are asking for cells connected to the next plane
      const vtkm::Id offset = this->CellsPerPlane;
      return this->PlanePortal.Get(this->PlaneOffset + index) + offset;
    }

    if (index < this->PlaneNumComponents)
    {
      //we are asking for cells connected to the previous plane
      const vtkm::Id offset = this->Plane * this->CellsPerPlane;
      return this->PlanePortal.Get(this->PlaneOffset + index) + offset;
    }
    else
    {
      //we are asking for cells connected to the next plane
      const vtkm::Id offset = this->NPlane * this->CellsPerPlane;
      return this->PlanePortal.Get(this->PlaneOffset + (index - this->PlaneNumComponents)) + offset;
    }
  }

  template <typename T, vtkm::IdComponent DestSize>
  VTKM_EXEC void CopyInto(vtkm::Vec<T, DestSize>& dest) const
  {
    vtkm::IdComponent numComponents = vtkm::Min(DestSize, (this->PlaneNumComponents * 2));
    for (vtkm::IdComponent index = 0; index < numComponents; index++)
    {
      dest[index] = this->operator[](index);
    }
  }

  VTKM_EXEC
  vtkm::Id GetNumberOfComponents() const
  {
    if (this->PeriodicBoundary)
    {
      return this->PlaneNumComponents * 2;
    }
    else
    {
      if (this->Plane == 0 || this->NPlane == 0)
      {
        return this->PlaneNumComponents;
      }
      else
      {
        return this->PlaneNumComponents * 2;
      }
    }
  }

  PortalType PlanePortal;
  vtkm::Int32 PlaneNumComponents;
  vtkm::Int32 PlaneOffset;
  vtkm::Int32 CellsPerPlane;
  vtkm::Int32 Plane;
  vtkm::Int32 NPlane;
  bool PeriodicBoundary;
};
}
}

#endif
