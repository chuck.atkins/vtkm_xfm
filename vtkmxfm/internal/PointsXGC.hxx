
#include "vtkmxfm/internal/PointsXGC.h"

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ErrorBadType.h>
#include <vtkm/cont/ErrorInternal.h>

#include <vtkm/BaseComponent.h>

namespace vtkm
{
namespace exec
{

template <typename PortalType>
typename ArrayPortalPointsXGC<PortalType>::ValueType
ArrayPortalPointsXGC<PortalType>::ArrayPortalPointsXGC::Get(vtkm::Id index) const
{
  const vtkm::Id realIdx = (index * 2) % this->NumberOfValues;
  const vtkm::Id whichPlane = (index * 2) / this->NumberOfValues;
  const auto phi = whichPlane * (vtkm::TwoPi() / this->NumberOfPlanes);

  auto r = this->Portal.Get(realIdx);
  auto z = this->Portal.Get(realIdx + 1);
  if (this->UseCylindrical)
  {
    return ValueType(r, phi, z);
  }
  else
  {
    return ValueType(r * vtkm::Cos(phi), r * vtkm::Sin(phi), z);
  }
}

template <typename PortalType>
typename ArrayPortalPointsXGC<PortalType>::ValueType
ArrayPortalPointsXGC<PortalType>::ArrayPortalPointsXGC::Get(vtkm::Id2 index) const
{
  const vtkm::Id realIdx = (index[0] * 2);
  const vtkm::Id whichPlane = index[1];
  const auto phi = whichPlane * (vtkm::TwoPi() / this->NumberOfPlanes);

  auto r = this->Portal.Get(realIdx);
  auto z = this->Portal.Get(realIdx + 1);
  if (this->UseCylindrical)
  {
    return ValueType(r, phi, z);
  }
  else
  {
    return ValueType(r * vtkm::Cos(phi), r * vtkm::Sin(phi), z);
  }
}

template <typename PortalType>
vtkm::Vec<typename ArrayPortalPointsXGC<PortalType>::ValueType,6>
ArrayPortalPointsXGC<PortalType>::ArrayPortalPointsXGC::GetWedge(const ToroidIndices& index) const
{
  const auto phi = index.Plane * (vtkm::TwoPi() / this->NumberOfPlanes);
  const auto nphi = (index.NPlane) * (vtkm::TwoPi() / this->NumberOfPlanes);

  vtkm::Vec<ValueType,6> result;
  for(int i=0; i < 3; ++i)
  {
    const vtkm::Id realIdx = index.PointIds[i]*2;
    auto r = this->Portal.Get(realIdx);
    auto z = this->Portal.Get(realIdx + 1);
    if (this->UseCylindrical)
    {
      result[i] = ValueType(r, phi, z);
      result[i+3] = ValueType(r, nphi, z);
    }
    else
    {
      result[i] = ValueType(r * vtkm::Cos(phi), r * vtkm::Sin(phi), z);
      result[i+3] = ValueType(r * vtkm::Cos(nphi), r * vtkm::Sin(nphi), z);
    }
  }
  return result;
}


}
}
