
#ifndef vtkmxfm_internal_FetchXGC_h
#define vtkmxfm_internal_FetchXGC_h

#include "vtkmxfm/internal/ConnectivityToroid.h"
#include "vtkmxfm/internal/PointsXGC.h"
#include <vtkm/exec/arg/FromIndices.h>
#include <vtkm/exec/arg/FetchTagArrayDirectIn.h>
#include <vtkm/exec/arg/FetchTagArrayTopologyMapIn.h>

//optimized fetches for ArrayPortalPointsXGC for
// - 3D Scheduling
// - WorkletNeighboorhood
namespace vtkm
{
namespace exec
{
namespace arg
{

//Optimized fetch for point ids when iterating the cells ConnectivityToroid
template <typename FetchType, typename Device, typename ExecObjectType>
struct Fetch<FetchType,
             vtkm::exec::arg::AspectTagFromIndices,
             vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ConnectivityToroid<Device>>,
             ExecObjectType>
{
  using ThreadIndicesType = vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ConnectivityToroid<Device>>;

  using ValueType = vtkm::Vec<vtkm::Id,6>;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices, const ExecObjectType&) const
  {
    // std::cout << "opimized fetch for point ids" << std::endl;
    const auto& toriod = indices.GetIndicesFrom();
    const vtkm::Id offset1 = (toriod.Plane * toriod.PointsPerPlane);
    const vtkm::Id offset2 = (toriod.NPlane * toriod.PointsPerPlane);
    ValueType result;
    result[0] = offset1 + toriod.PointIds[0];
    result[1] = offset1 + toriod.PointIds[1];
    result[2] = offset1 + toriod.PointIds[2];
    result[3] = offset2 + toriod.PointIds[0];
    result[4] = offset2 + toriod.PointIds[1];
    result[5] = offset2 + toriod.PointIds[2];
    return result;
  }

  VTKM_EXEC
  void Store(const ThreadIndicesType&, const ExecObjectType&, const ValueType&) const
  {
    // Store is a no-op.
  }
};

//Optimized fetch for point arrays when iterating the cells ConnectivityToroid
template <typename Device, typename PortalType>
struct Fetch<vtkm::exec::arg::FetchTagArrayTopologyMapIn,
             vtkm::exec::arg::AspectTagDefault,
             vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ConnectivityToroid<Device>>,
             PortalType>
{
  using ThreadIndicesType =
    vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ConnectivityToroid<Device>>;
  using ValueType = vtkm::Vec<typename PortalType::ValueType, 6>;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices, const PortalType& portal)
  {
    // std::cout << "opimized fetch for point values" << std::endl;
    const auto& toriod = indices.GetIndicesFrom();
    const vtkm::Id offset1 = (toriod.Plane * toriod.PointsPerPlane);
    const vtkm::Id offset2 = (toriod.NPlane * toriod.PointsPerPlane);
    ValueType result;
    result[0] = portal.Get(offset1 + toriod.PointIds[0]);
    result[1] = portal.Get(offset1 + toriod.PointIds[1]);
    result[2] = portal.Get(offset1 + toriod.PointIds[2]);
    result[3] = portal.Get(offset2 + toriod.PointIds[0]);
    result[4] = portal.Get(offset2 + toriod.PointIds[1]);
    result[5] = portal.Get(offset2 + toriod.PointIds[2]);
    return result;
  }

  VTKM_EXEC
  void Store(const ThreadIndicesType&, const PortalType&, const ValueType&) const
  {
    // Store is a no-op for this fetch.
  }
};

//Optimized fetch for point coordinates when iterating the cells of ConnectivityToroid
template <typename Device, typename T>
struct Fetch<vtkm::exec::arg::FetchTagArrayTopologyMapIn,
             vtkm::exec::arg::AspectTagDefault,
             vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ConnectivityToroid<Device>>,
             vtkm::exec::ArrayPortalPointsXGC<T>>

{
  using ThreadIndicesType =
    vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ConnectivityToroid<Device>>;
  using ValueType = vtkm::Vec<typename vtkm::exec::ArrayPortalPointsXGC<T>::ValueType, 6>;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices,
                 const vtkm::exec::ArrayPortalPointsXGC<T>& points)
  {
    // std::cout << "opimized fetch for point coordinates" << std::endl;
    return points.GetWedge(indices.GetIndicesFrom());
  }

  VTKM_EXEC
  void Store(const ThreadIndicesType&,
             const vtkm::exec::ArrayPortalPointsXGC<T>&,
             const ValueType&) const
  {
    // Store is a no-op for this fetch.
  }
};

//Optimized fetch for point coordinates when iterating the points of ConnectivityToroid
template <typename Device, typename T>
struct Fetch<vtkm::exec::arg::FetchTagArrayDirectIn,
             vtkm::exec::arg::AspectTagDefault,
             vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ReverseConnectivityToroid<Device>>,
             vtkm::exec::ArrayPortalPointsXGC<T>>

{
  using ThreadIndicesType =
    vtkm::exec::arg::ThreadIndicesTopologyMap<vtkm::exec::ReverseConnectivityToroid<Device>>;
  using ValueType = typename vtkm::exec::ArrayPortalPointsXGC<T>::ValueType;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices,
                 const vtkm::exec::ArrayPortalPointsXGC<T>& points)
  {
    // std::cout << "opimized fetch for point coordinates" << std::endl;
    return points.Get(indices.GetIndexLogical());
  }

  VTKM_EXEC
  void Store(const ThreadIndicesType&,
             const vtkm::exec::ArrayPortalPointsXGC<T>&,
             const ValueType&) const
  {
    // Store is a no-op for this fetch.
  }
};
}
}
}


#endif
