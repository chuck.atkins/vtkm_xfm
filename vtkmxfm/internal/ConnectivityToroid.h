#ifndef vtkmxfm_internal_ConnectivityToroid_h
#define vtkmxfm_internal_ConnectivityToroid_h

#include "vtkmxfm/vtkm_xfm_export.h"

#include <vtkm/CellShape.h>
#include <vtkm/cont/ArrayHandle.h>

#include <vtkm/exec/arg/ThreadIndicesTopologyMap.h>

#include "vtkmxfm/internal/ToroidIndices.h"

namespace vtkm
{
namespace exec
{

template <typename Device>
struct VTKM_ALWAYS_EXPORT ConnectivityToroid
{
  using ConnectivityHandleType = vtkm::cont::ArrayHandle<vtkm::Int32>;
  using ConnectivityPortalType =
    typename ConnectivityHandleType::template ExecutionTypes<Device>::PortalConst;

  using SchedulingRangeType = vtkm::Id2;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ConnectivityToroid();

  ConnectivityToroid(const ConnectivityPortalType& connPortal,
                     vtkm::Int32 cellsPerPlane,
                     vtkm::Int32 pointsPerPlane,
                     vtkm::Int32 numPlanes,
                     bool periodic = true);

  VTKM_EXEC
  vtkm::Id GetNumberOfElements() const;

  using CellShapeTag = vtkm::CellShapeTagWedge;

  VTKM_EXEC
  CellShapeTag GetCellShape(vtkm::Id index) const { return CellShapeTag(); }

  using IndicesType = vtkm::exec::ToroidIndices;

  VTKM_EXEC
  IndicesType GetIndices(vtkm::Id index) const;

  VTKM_EXEC
  IndicesType GetIndices(const vtkm::Id2& index) const;

  VTKM_EXEC
  vtkm::Id LogicalToFlatToIndex(const vtkm::Id2& index) const
  {
    return index[0] + (index[1] * this->NumberOfCellsPerPlane);
  };

  VTKM_EXEC
  vtkm::Id2 FlatToLogicalToIndex(vtkm::Id index) const
  {
    const vtkm::Id cellId = index % this->NumberOfCellsPerPlane;
    const vtkm::Id plane = index / this->NumberOfCellsPerPlane;
    return vtkm::Id2(cellId, plane);
  }

  ConnectivityPortalType Connectivity;
  vtkm::Int32 NumberOfCellsPerPlane;
  vtkm::Int32 NumberOfPointsPerPlane;
  vtkm::Int32 NumberOfPlanes;
  bool PeriodicBoundary;
};

template <typename Device>
struct VTKM_ALWAYS_EXPORT ReverseConnectivityToroid
{
  using ConnectivityHandleType = vtkm::cont::ArrayHandle<vtkm::Int32>;
  using NumIndicesHandleType = vtkm::cont::ArrayHandle<vtkm::IdComponent>;
  using OffsetHandleType = vtkm::cont::ArrayHandle<vtkm::Int32>;

  using ConnectivityPortalType =
    typename ConnectivityHandleType::template ExecutionTypes<Device>::PortalConst;

  using IndexOffsetPortalType =
    typename OffsetHandleType::template ExecutionTypes<Device>::PortalConst;

  using NumIndicesPortalType =
    typename NumIndicesHandleType::template ExecutionTypes<Device>::PortalConst;

  using SchedulingRangeType = vtkm::Id2;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ReverseConnectivityToroid();

  ReverseConnectivityToroid(const ConnectivityPortalType& connPortal,
                            const NumIndicesPortalType& numIndicesPortal,
                            const IndexOffsetPortalType& indexOffsetPortal,
                            vtkm::Int32 cellsPerPlane,
                            vtkm::Int32 pointsPerPlane,
                            vtkm::Int32 numPlanes,
                            bool periodic = true);

  VTKM_EXEC
  vtkm::Id GetNumberOfElements() const;

  using CellShapeTag = vtkm::CellShapeTagVertex;

  VTKM_EXEC
  CellShapeTag GetCellShape(vtkm::Id) const { return vtkm::CellShapeTagVertex(); }

  using IndicesType = vtkm::exec::ReverseToroidIndices<ConnectivityPortalType>;

  /// Returns a Vec-like object containing the indices for the given index.
  /// The object returned is not an actual array, but rather an object that
  /// loads the indices lazily out of the connectivity array. This prevents
  /// us from having to know the number of indices at compile time.
  ///
  VTKM_EXEC
  IndicesType GetIndices(vtkm::Id index) const;

  VTKM_EXEC
  IndicesType GetIndices(const vtkm::Id2& index) const;

  VTKM_EXEC
  vtkm::Id LogicalToFlatToIndex(const vtkm::Id2& index) const
  {
    return index[0] + (index[1] * this->NumberOfPointsPerPlane);
  };

  VTKM_EXEC
  vtkm::Id2 FlatToLogicalToIndex(vtkm::Id index) const
  {
    const vtkm::Id vertId = index % this->NumberOfPointsPerPlane;
    const vtkm::Id plane = index / this->NumberOfPointsPerPlane;
    return vtkm::Id2(vertId, plane);
  }

  ConnectivityPortalType Connectivity;
  NumIndicesPortalType NumIndices;
  IndexOffsetPortalType IndexOffsets;
  vtkm::Int32 NumberOfCellsPerPlane;
  vtkm::Int32 NumberOfPointsPerPlane;
  vtkm::Int32 NumberOfPlanes;
  bool PeriodicBoundary;
};
}
} // namespace vtkm::exec

#include <vtkm/cont/serial/DeviceAdapterSerial.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>
// template classes we want to compile only once
namespace vtkm
{
namespace exec
{
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ConnectivityToroid<vtkm::cont::DeviceAdapterTagSerial>;
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ReverseConnectivityToroid<vtkm::cont::DeviceAdapterTagSerial>;
#ifdef VTKM_ENABLE_TBB
extern template struct VTKM_XFM_TEMPLATE_EXPORT ConnectivityToroid<vtkm::cont::DeviceAdapterTagTBB>;
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ReverseConnectivityToroid<vtkm::cont::DeviceAdapterTagTBB>;
#endif
}
}
#endif
