
#ifndef vtkmxfm_internal_PointsXGC_h
#define vtkmxfm_internal_PointsXGC_h

#include "vtkmxfm/vtkm_xfm_export.h"

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ErrorBadType.h>
#include <vtkm/cont/ErrorInternal.h>

#include <vtkm/BaseComponent.h>

#include "vtkmxfm/internal/ToroidIndices.h"

namespace vtkm
{
namespace exec
{

template <typename PortalType>
struct VTKM_ALWAYS_EXPORT ArrayPortalPointsXGC
{
  using ValueType = vtkm::Vec<typename PortalType::ValueType, 3>;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ArrayPortalPointsXGC()
    : Portal()
    , NumberOfValues(0)
    , NumberOfPlanes(0)
    , UseCylindrical(false) {};

  ArrayPortalPointsXGC(const PortalType& p, vtkm::Int32 numOfValues, vtkm::Int32 numOfPlanes,
    bool cylindrical = false)
    : Portal(p)
    , NumberOfValues(numOfValues)
    , NumberOfPlanes(numOfPlanes)
    , UseCylindrical(cylindrical)
  {
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  vtkm::Id GetNumberOfValues() const { return ((NumberOfValues/2) * static_cast<vtkm::Id>(NumberOfPlanes)) ; }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ValueType Get(vtkm::Id index) const;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ValueType Get(vtkm::Id2 index) const;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  vtkm::Vec<ValueType,6> GetWedge(const ToroidIndices& index) const;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  void Set(vtkm::Id vtkmNotUsed(index), const ValueType& vtkmNotUsed(value)) const {}

  PortalType Portal;
  vtkm::Int32 NumberOfValues;
  vtkm::Int32 NumberOfPlanes;
  bool UseCylindrical;

};
}
}

namespace vtkm
{
namespace cont
{
namespace internal
{
struct VTKM_ALWAYS_EXPORT StorageTagPointsXGC
{
};

template <typename T>
class VTKM_ALWAYS_EXPORT Storage<T, internal::StorageTagPointsXGC>
{
  using BaseT = typename BaseComponent<T>::Type;
  using HandleType = vtkm::cont::ArrayHandle<BaseT>;
  using TPortalType = typename HandleType::PortalConstControl;

public:
  using ValueType = T;

  // This is meant to be invalid. Because XGC point arrays are read only, you
  // should only be able to use the const version.
  struct PortalType
  {
    using ValueType = void*;
    using IteratorType = void*;
  };

  using PortalConstType = vtkm::exec::ArrayPortalPointsXGC<TPortalType>;

  Storage()
    : Array()
    , Length(-1)
    , NumberOfPlanes(0)
  {
  }

  Storage(const BaseT* array, vtkm::Id arrayLength, vtkm::Int32 numberOfPlanes,
    bool cylindrical = false)
    : Array(vtkm::cont::make_ArrayHandle(array, arrayLength))
    , Length(static_cast<vtkm::Int32>(arrayLength))
    , NumberOfPlanes(numberOfPlanes)
    , UseCylindrical(cylindrical)
  {
    VTKM_ASSERT(this->Length >= 0);
  }

  PortalType GetPortal() { return PortalType{}; }

  PortalConstType GetPortalConst() const
  {
    VTKM_ASSERT(this->Length >= 0);
    return PortalConstType(this->Array.GetPortalConstControl(), this->Length, this->NumberOfPlanes, this->UseCylindrical);
  }

  vtkm::Id GetNumberOfValues() const
  {
    VTKM_ASSERT(this->Length >= 0);
    return (this->Length/2) * static_cast<vtkm::Id>(this->NumberOfPlanes);
  }

  void Allocate(vtkm::Id vtkmNotUsed(numberOfValues))
  {
    throw vtkm::cont::ErrorBadType(
      "ArrayHandleXGCPointCoordinates is read only. It cannot be allocated.");
  }

  void Shrink(vtkm::Id vtkmNotUsed(numberOfValues))
  {
    throw vtkm::cont::ErrorBadType(
      "ArrayHandleXGCPointCoordinates is read only. It cannot shrink.");
  }

  void ReleaseResources()
  {
    // This request is ignored since we don't own the memory that was past
    // to us
  }

  vtkm::cont::ArrayHandle<BaseT> Array;
  vtkm::Int32 Length;
  vtkm::Int32 NumberOfPlanes;
  bool UseCylindrical;
};

template <typename T, typename Device>
class VTKM_ALWAYS_EXPORT ArrayTransfer<T, internal::StorageTagPointsXGC, Device>
{
  using BaseT = typename BaseComponent<T>::Type;
  using TPortalType = decltype(vtkm::cont::ArrayHandle<BaseT>{}.PrepareForInput(Device{}));

public:
  using ValueType = T;
  using StorageType = vtkm::cont::internal::Storage<T, internal::StorageTagPointsXGC>;

  using PortalControl = typename StorageType::PortalType;
  using PortalConstControl = typename StorageType::PortalConstType;

  //meant to be an invalid writeable execution portal
  using PortalExecution = typename StorageType::PortalType;

  using PortalConstExecution = vtkm::exec::ArrayPortalPointsXGC<TPortalType>;

  VTKM_CONT
  ArrayTransfer(StorageType* storage)
    : ControlData(storage)
  {
  }
  vtkm::Id GetNumberOfValues() const
  {
    return this->ControlData->GetNumberOfValues();
  }

  VTKM_CONT
  PortalConstExecution PrepareForInput(bool vtkmNotUsed(updateData))
  {
    return PortalConstExecution(this->ControlData->Array.PrepareForInput(Device()),
                                this->ControlData->Length,
                                this->ControlData->NumberOfPlanes,
                                this->ControlData->UseCylindrical);
  }

  VTKM_CONT
  PortalExecution PrepareForInPlace(bool& vtkmNotUsed(updateData))
  {
    throw vtkm::cont::ErrorBadType("ArrayHandleXGCPointCoordinates read only. "
                                   "Cannot be used for in-place operations.");
  }

  VTKM_CONT
  PortalExecution PrepareForOutput(vtkm::Id vtkmNotUsed(numberOfValues))
  {
    throw vtkm::cont::ErrorBadType(
      "ArrayHandleXGCPointCoordinates read only. Cannot be used as output.");
  }

  VTKM_CONT
  void RetrieveOutputData(StorageType* vtkmNotUsed(storage)) const
  {
    throw vtkm::cont::ErrorInternal(
      "ArrayHandleXGCPointCoordinates read only. "
      "There should be no occurance of the ArrayHandle trying to pull "
      "data from the execution environment.");
  }

  VTKM_CONT
  void Shrink(vtkm::Id vtkmNotUsed(numberOfValues))
  {
    throw vtkm::cont::ErrorBadType("ArrayHandleXGCPointCoordinates read only. Cannot shrink.");
  }

  VTKM_CONT
  void ReleaseResources()
  {
    // This request is ignored since we don't own the memory that was past
    // to us
  }

private:
  const StorageType* const ControlData;
};
}
}
}

// template classes we want to compile only once
namespace vtkm { namespace exec {
extern template struct VTKM_XFM_TEMPLATE_EXPORT ArrayPortalPointsXGC<cont::internal::ArrayPortalFromIterators<float const*, void>>;
extern template struct VTKM_XFM_TEMPLATE_EXPORT ArrayPortalPointsXGC<cont::internal::ArrayPortalFromIterators<double const*, void>>;
} }

#include <vtkm/cont/serial/DeviceAdapterSerial.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>
namespace vtkm { namespace cont { namespace internal {
extern template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<float,3>, StorageTagPointsXGC>;
extern template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<double,3>, StorageTagPointsXGC>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExecutionManagerBase<vtkm::Vec<float, 3>, StorageTagPointsXGC>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExecutionManagerBase<vtkm::Vec<double, 3>, StorageTagPointsXGC>;

extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayTransfer<vtkm::Vec<float,3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayTransfer<vtkm::Vec<double,3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagSerial>;

#ifdef VTKM_ENABLE_TBB
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayTransfer<vtkm::Vec<float,3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayTransfer<vtkm::Vec<double,3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
extern template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagPointsXGC, DeviceAdapterTagTBB>;
#endif
} } }

#endif
