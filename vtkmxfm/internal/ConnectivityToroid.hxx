
#include "vtkmxfm/internal/ConnectivityToroid.h"

namespace vtkm
{
namespace exec
{

template <typename Device>
ConnectivityToroid<Device>::ConnectivityToroid()
{
}

template <typename Device>
ConnectivityToroid<Device>::ConnectivityToroid(const ConnectivityPortalType& portal,
                                               vtkm::Int32 cellsPerPlane,
                                               vtkm::Int32 pointsPerPlane,
                                               vtkm::Int32 numPlanes,
                                               bool periodic)
  : Connectivity(portal)
  , NumberOfCellsPerPlane(cellsPerPlane)
  , NumberOfPointsPerPlane(pointsPerPlane)
  , NumberOfPlanes(numPlanes)
  , PeriodicBoundary(periodic)
{
}

template <typename Device>
vtkm::Id ConnectivityToroid<Device>::GetNumberOfElements() const
{
  if (this->PeriodicBoundary)
  {
    return static_cast<vtkm::Id>(this->NumberOfCellsPerPlane) * this->NumberOfPlanes;
  }
  else
  {
    return static_cast<vtkm::Id>(this->NumberOfCellsPerPlane) * (this->NumberOfPlanes - 1);
  }
}

template <typename Device>
ToroidIndices ConnectivityToroid<Device>::GetIndices(vtkm::Id index) const
{
  const vtkm::Id cellId = index % this->NumberOfCellsPerPlane;
  const vtkm::Id plane = index / this->NumberOfCellsPerPlane;
  return this->GetIndices(vtkm::Id2(cellId, plane));
}

template <typename Device>
ToroidIndices ConnectivityToroid<Device>::GetIndices(const vtkm::Id2& index) const
{
  const vtkm::Id3 pointIds(this->Connectivity.Get(index[0] * 3),
                           this->Connectivity.Get((index[0] * 3) + 1),
                           this->Connectivity.Get((index[0] * 3) + 2));
  const vtkm::Id nextPlaneId = (index[1] < (this->NumberOfPlanes - 1)) ? index[1] + 1 : 0;

  return ToroidIndices(this->NumberOfPointsPerPlane, index[1], nextPlaneId, pointIds);
}


template <typename Device>
ReverseConnectivityToroid<Device>::ReverseConnectivityToroid()
{
}

template <typename Device>
ReverseConnectivityToroid<Device>::ReverseConnectivityToroid(
  const ConnectivityPortalType& connPortal,
  const NumIndicesPortalType& numIndicesPortal,
  const IndexOffsetPortalType& indexOffsetPortal,
  vtkm::Int32 cellsPerPlane,
  vtkm::Int32 pointsPerPlane,
  vtkm::Int32 numPlanes,
  bool periodic)
  : Connectivity(connPortal)
  , NumIndices(numIndicesPortal)
  , IndexOffsets(indexOffsetPortal)
  , NumberOfCellsPerPlane(cellsPerPlane)
  , NumberOfPointsPerPlane(pointsPerPlane)
  , NumberOfPlanes(numPlanes)
  , PeriodicBoundary(periodic)
{
}

template <typename Device>
typename ReverseConnectivityToroid<Device>::IndicesType
ReverseConnectivityToroid<Device>::GetIndices(vtkm::Id index) const
{
  const vtkm::Id vertId = index % this->NumberOfPointsPerPlane;
  const vtkm::Id plane = index / this->NumberOfPointsPerPlane;
  return this->GetIndices(vtkm::Id2(vertId, plane));
}

template <typename Device>
typename ReverseConnectivityToroid<Device>::IndicesType
ReverseConnectivityToroid<Device>::GetIndices(const vtkm::Id2& index) const
{
  //need to compute the current plane id and the previous plane id
  const vtkm::Id offset = this->IndexOffsets.Get(index[0]);
  const vtkm::IdComponent length = this->NumIndices.Get(index[0]);

  const vtkm::Id prevPlaneId = (index[1] == 0) ? (this->NumberOfPlanes - 1) : index[1] - 1;
  return IndicesType(this->Connectivity, length, offset, this->NumberOfCellsPerPlane, prevPlaneId, index[1], this->PeriodicBoundary);
}

}
}
