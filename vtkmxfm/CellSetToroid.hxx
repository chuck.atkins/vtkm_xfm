
#include "CellSetToroid.h"

#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>

namespace
{
class ComputeToroidReverseMapping : public vtkm::worklet::WorkletMapField
{
public:
  typedef void ControlSignature(FieldIn<IdType> cellIndex,
                                WholeArrayIn<> connectivity,
                                WholeArrayOut<> cellIds);
  typedef void ExecutionSignature(_1, _2, _3);


  VTKM_SUPPRESS_EXEC_WARNINGS
  template <typename ConnPortalType, typename PortalType>
  VTKM_EXEC void operator()(vtkm::Id cellId,
                            ConnPortalType&& connectivity,
                            PortalType&& pointIdValue) const
  {
     //3 as we are building the connectivity for triangles
    const vtkm::Id offset = 3 * cellId;
    pointIdValue.Set(offset, cellId);
    pointIdValue.Set(offset + 1, cellId);
    pointIdValue.Set(offset + 2, cellId);
  }
};
} //namespace


namespace vtkm
{
namespace cont
{

template <typename Device>
typename vtkm::exec::ConnectivityToroid<Device> CellSetToroid::PrepareForInput(
  Device device,
  vtkm::TopologyElementTagPoint,
  vtkm::TopologyElementTagCell) const
{
  return vtkm::exec::ConnectivityToroid<Device>(this->Connectivity.PrepareForInput(device),
                                                this->NumberOfCellsPerPlane,
                                                this->NumberOfPointsPerPlane,
                                                this->NumberOfPlanes,
                                                this->PeriodicBoundary);
}

template <typename Device>
typename vtkm::exec::ReverseConnectivityToroid<Device> CellSetToroid::PrepareForInput(
  Device device,
  vtkm::TopologyElementTagCell,
  vtkm::TopologyElementTagPoint) const
{
  // Build the reverse connectivity for the points of the toroid.
  // The reverse connectivity is first computed for all points in a the first plane to
  // what triangle cells that use those points. This intermediate representation can
  // than be used to compute the wedge cells for points on any plane by a simple
  // transformation.
  //
  if (!this->ReverseConnectivityBuilt)
  {
    using Algorithm = vtkm::cont::DeviceAdapterAlgorithm<Device>;

    // create a mapping of where each key is the point id and the value
    // is the cell id. We
    const vtkm::Id numberOfPointsPerCell = 3;
    const vtkm::Id rconnSize = this->NumberOfCellsPerPlane * numberOfPointsPerCell;

    vtkm::cont::ArrayHandle<vtkm::Int32> pointIdKey;
    Algorithm::Copy(this->Connectivity, pointIdKey);

    vtkm::worklet::DispatcherMapField<ComputeToroidReverseMapping, Device> dispatcher;
    this->RConn.Allocate(rconnSize);
    dispatcher.Invoke(
      vtkm::cont::make_ArrayHandleCounting<vtkm::Id>(0, 1, this->NumberOfCellsPerPlane),
      this->Connectivity,
      this->RConn);

    Algorithm::SortByKey(pointIdKey, this->RConn);

    // now we can compute the NumIndices
    vtkm::cont::ArrayHandle<vtkm::Int32> reducedKeys;
    Algorithm::ReduceByKey(
      pointIdKey,
      vtkm::cont::make_ArrayHandleConstant(vtkm::Int32(1), static_cast<vtkm::Int32>(rconnSize)),
      reducedKeys,
      this->RNumIndices,
      vtkm::Add{});

    // than a scan exclusive will give us the index offsets
    Algorithm::ScanExclusive(this->RNumIndices, this->RIndexOffsets);

    this->ReverseConnectivityBuilt = true;
  }

  if (this->PeriodicBoundary)
  {
    return vtkm::exec::ReverseConnectivityToroid<Device>(
      this->RConn.PrepareForInput(device),
      this->RNumIndices.PrepareForInput(device),
      this->RIndexOffsets.PrepareForInput(device),
      this->NumberOfCellsPerPlane,
      this->NumberOfPointsPerPlane,
      this->NumberOfPlanes);
  }
  else
  {
    return vtkm::exec::ReverseConnectivityToroid<Device>(
      this->RConn.PrepareForInput(device),
      this->RNumIndices.PrepareForInput(device),
      this->RIndexOffsets.PrepareForInput(device),
      this->NumberOfCellsPerPlane,
      this->NumberOfPointsPerPlane,
      this->NumberOfPlanes,
      false);
  }
}
}
}
