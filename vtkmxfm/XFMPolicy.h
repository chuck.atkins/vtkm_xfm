//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2012 Sandia Corporation.
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//=============================================================================

#ifndef XFMPolicy_h
#define XFMPolicy_h

#include "vtkmxfm/ArrayHandleXGCPointCoordinates.h"
#include "vtkmxfm/CellSetToroid.h"

#include <vtkm/filter/PolicyDefault.h>
#include <vtkm/ListTag.h>

struct VTKM_ALWAYS_EXPORT xfmUnstructuredCellSets : vtkm::ListTagBase<vtkm::cont::CellSetToroid>
{
};

//Todo: add in Cylinder storage tag when it is written
struct VTKM_ALWAYS_EXPORT xfmCoordinateStorage : vtkm::ListTagBase<vtkm::cont::StorageTagBasic, vtkm::cont::internal::StorageTagPointsXGC>
{
};

struct VTKM_ALWAYS_EXPORT xfmPolicy : vtkm::filter::PolicyBase<xfmPolicy>
{
public:
  using UnstructuredCellSetList = xfmUnstructuredCellSets;
  using AllCellSetList = xfmUnstructuredCellSets;
  using CoordinateStorageList = xfmCoordinateStorage;
};

#endif
