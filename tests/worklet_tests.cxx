#include "vtkmxfm/vtkmxfm.h"

#include <vtkm/cont/ArrayHandleConstant.h>

#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/CellAverage.h>
#include <vtkm/worklet/PointAverage.h>

#include <vtkm/cont/testing/Testing.h>

#include "tests/example_data.h"

struct CopyCoordinates : public vtkm::worklet::WorkletMapPointToCell
{
  typedef void ControlSignature(CellSetIn, FieldInPoint<Vec3>, FieldOutCell<>);
  typedef _3 ExecutionSignature(_2);

  template<typename T>
  T&& operator()(T&& t) const { return std::forward<T>(t); }
};

struct CopyValue : public vtkm::worklet::WorkletMapField
{
  typedef void ControlSignature(FieldIn<>, FieldOut<>);
  typedef _2 ExecutionSignature(_1);

  template<typename T>
  T&& operator()(T&& t) const { return std::forward<T>(t); }
};

struct CopyCoordinatesC2P : public vtkm::worklet::WorkletMapCellToPoint
{
  typedef void ControlSignature(CellSetIn, FieldInPoint<Vec3>, FieldOutPoint<Vec3>);
  typedef _3 ExecutionSignature(CellShape, _2);

  template <typename T>
  T&& operator()(vtkm::CellShapeTagVertex, T&& t) const
  {
    return std::forward<T>(t);
  }
};


template<typename T, typename S>
void verify_coordinates(vtkm::cont::ArrayHandle<vtkm::Vec<T,6>, S> const& handle, vtkm::Id expectedLen)
{
  auto portal = handle.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == expectedLen,
                   "topology portal size is incorrect");
  for (vtkm::Id i = 0; i < expectedLen-1; ++i)
  {
    auto v = portal.Get(i);

    std::size_t offset = (i * (points_rz.size()/2));
    std::size_t offset2 = ((i+1) * (points_rz.size()/2));
    vtkm::Vec<T,6> e;
    e[0] = T(correct_x_coords[offset + topology[0]], correct_y_coords[offset + topology[0]], correct_z_coords[offset + topology[0]]);
    e[1] = T(correct_x_coords[offset + topology[1]], correct_y_coords[offset + topology[1]], correct_z_coords[offset + topology[1]]);
    e[2] = T(correct_x_coords[offset + topology[2]], correct_y_coords[offset + topology[2]], correct_z_coords[offset + topology[2]]);
    e[3] = T(correct_x_coords[offset2 + topology[0]], correct_y_coords[offset2 + topology[0]], correct_z_coords[offset2 + topology[0]]);
    e[4] = T(correct_x_coords[offset2 + topology[1]], correct_y_coords[offset2 + topology[1]], correct_z_coords[offset2 + topology[1]]);
    e[5] = T(correct_x_coords[offset2 + topology[2]], correct_y_coords[offset2 + topology[2]], correct_z_coords[offset2 + topology[2]]);

    VTKM_TEST_ASSERT(test_equal(v,e), "incorrect extraction of point coordinates of cells");
  }
}

template<typename T, typename S>
void verify_coordinates(vtkm::cont::ArrayHandle<vtkm::Vec<T,3>, S> const& handle)
{
  auto portal = handle.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == static_cast<vtkm::Id>(correct_x_coords.size()),
                   "coordinate portal size is incorrect");

  for (vtkm::Id i = 0; i < handle.GetNumberOfValues(); ++i)
  {
    auto v = portal.Get(i);
    auto e = vtkm::make_Vec(correct_x_coords[static_cast<std::size_t>(i)],
                            correct_y_coords[static_cast<std::size_t>(i)],
                            correct_z_coords[static_cast<std::size_t>(i)]);
    // std::cout << std::setprecision(4) << "computed " << v << " expected " << e << std::endl;
    VTKM_TEST_ASSERT(test_equal(v,e), "incorrect conversion to Cartesian space");
  }
}

int main(int argc, char** argv)
{
  const std::size_t numPlanes = 8;

  auto coords = vtkm::cont::make_ArrayHandleXGCPointCoordinates(points_rz, numPlanes);
  auto cells = vtkm::cont::make_CellSetToroid(topology, coords);

  // Verify we can get point coordinates
  {
  vtkm::cont::ArrayHandle< vtkm::Vec< vtkm::Vec<float,3>,6> > output;
  vtkm::worklet::DispatcherMapTopology< CopyCoordinates > dispatcher;
  dispatcher.Invoke(cells, coords, output);
  verify_coordinates(output, 8);
  }

  // verify that a constant value point field can be accessed
  {
  auto constantV = vtkm::cont::make_ArrayHandleConstant( 42.0f, coords.GetNumberOfValues() );
  vtkm::cont::ArrayHandle< vtkm::Float32 > output;

  vtkm::worklet::DispatcherMapTopology<vtkm::worklet::CellAverage> dispatcher;
  dispatcher.Invoke(cells, constantV, output);

  auto portal = output.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == cells.GetNumberOfCells(),
                   "cell output length from CellAverage is incorrect");
  for (vtkm::Id i = 0; i < portal.GetNumberOfValues(); ++i)
  {
    auto v = portal.Get(i);
    VTKM_TEST_ASSERT(v == 42.0f, "averaging of constant point field failed");
  }
  }

  // verify that a constant cell value can be accessed
  {
  auto constantV = vtkm::cont::make_ArrayHandleConstant( 42.0f, cells.GetNumberOfCells() );
  vtkm::cont::ArrayHandle< vtkm::Float32 > output;

  vtkm::worklet::DispatcherMapField<CopyValue> dispatcher;
  dispatcher.Invoke(constantV, output);

  auto portal = output.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == cells.GetNumberOfCells(),
                   "cell output length from CopyValue is incorrect");
  for (vtkm::Id i = 0; i < portal.GetNumberOfValues(); ++i)
  {
    auto v = portal.Get(i);
    VTKM_TEST_ASSERT(v == 42.0f, "copying of constant cell field failed");
  }
  }

  // verify that we can use point coordinates with WorkletMapCellToPoint worklets
  {
  vtkm::cont::ArrayHandle< vtkm::Vec<float,3> > output;
  vtkm::worklet::DispatcherMapTopology<CopyCoordinatesC2P> dispatcher;
  dispatcher.Invoke(cells, coords, output);
  verify_coordinates(output);
  }

  // verify that we can use point field with WorkletMapCellToPoint worklets
  {
  auto constantV = vtkm::cont::make_ArrayHandleConstant( 42.0f, cells.GetNumberOfCells() );
  vtkm::cont::ArrayHandle< vtkm::Float32 > output;
  vtkm::worklet::DispatcherMapTopology<vtkm::worklet::PointAverage> dispatcher;
  dispatcher.Invoke(cells, constantV, output);

  auto portal = output.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == cells.GetNumberOfPoints(),
                   "cell output length from PointAverage is incorrect");
  for (vtkm::Id i = 0; i < portal.GetNumberOfValues(); ++i)
  {
    auto v = portal.Get(i);
    VTKM_TEST_ASSERT(v == 42.0f, "averaging of constant cell field failed");
  }
  }

  return 0;
}



