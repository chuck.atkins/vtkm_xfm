
#include "vtkmxfm/vtkmxfm.h"

#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/DispatcherMapField.h>

#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/DispatcherMapField.h>

#include <vtkm/cont/testing/Testing.h>

#include "tests/example_data.h"


struct CopyValue : public vtkm::worklet::WorkletMapField
{
  typedef void ControlSignature(FieldIn<>, FieldOut<>);
  typedef _2 ExecutionSignature(_1);

  template<typename T>
  T&& operator()(T&& t) const { return std::forward<T>(t); }
};

template<typename T, typename S>
void verify_results(vtkm::cont::ArrayHandle<vtkm::Vec<T,3>, S> const& handle)
{
  auto portal = handle.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == static_cast<vtkm::Id>(correct_x_coords.size()),
                   "coordinate portal size is incorrect");

  for (vtkm::Id i = 0; i < handle.GetNumberOfValues(); ++i)
  {
    auto v = portal.Get(i);
    auto e = vtkm::make_Vec(correct_x_coords[static_cast<std::size_t>(i)],
                            correct_y_coords[static_cast<std::size_t>(i)],
                            correct_z_coords[static_cast<std::size_t>(i)]);
    // std::cout << std::setprecision(4) << "computed " << v << " expected " << e << std::endl;
    VTKM_TEST_ASSERT(test_equal(v,e), "incorrect conversion to Cartesian space");
  }
}


int main(int argc, char** argv)
{
  const int numPlanes = 8;

  auto coords = vtkm::cont::make_ArrayHandleXGCPointCoordinates(points_rz, numPlanes);

  VTKM_TEST_ASSERT(coords.GetNumberOfValues() ==
                     static_cast<vtkm::Id>(((points_rz.size() / 2) * numPlanes)),
                   "coordinate size is incorrect");

  // Verify first that control is correct
  verify_results(coords);

  // Verify 1d scheduling by doing a copy to a vtkm::ArrayHandle<Vec3>
  vtkm::cont::ArrayHandle<vtkm::Vec<float,3>> output1D;
  vtkm::worklet::DispatcherMapField<CopyValue> dispatcher;
  dispatcher.Invoke(coords,output1D);
  verify_results(output1D);

  return 0;
}
