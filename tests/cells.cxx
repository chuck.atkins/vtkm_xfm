
#include "vtkmxfm/vtkmxfm.h"

#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/DispatcherMapTopology.h>

#include <vtkm/cont/testing/Testing.h>

#include "tests/example_data.h"

struct CopyTopo : public vtkm::worklet::WorkletMapPointToCell
{
  typedef void ControlSignature(CellSetIn, FieldOutCell<>);
  typedef _2 ExecutionSignature(CellShape, PointIndices);

  template<typename T>
  T&& operator()(vtkm::CellShapeTagWedge, T&& t) const { return std::forward<T>(t); }
};

struct CopyReverseCellCount : public vtkm::worklet::WorkletMapCellToPoint
{
  typedef void ControlSignature(CellSetIn, FieldOutPoint<>);
  typedef _2 ExecutionSignature(CellShape, CellCount, CellIndices);

  template <typename T>
  vtkm::Int32 operator()(vtkm::CellShapeTagVertex, vtkm::IdComponent count, T&& t) const
  {
    bool valid = true;
    for (vtkm::IdComponent i = 0; i < count; ++i)
    {
      valid = valid && t[i] > 0;
    }
    return (valid && count == t.GetNumberOfComponents()) ? count : -1;
  }
};

template<typename T, typename S>
void verify_topo(vtkm::cont::ArrayHandle<vtkm::Vec<T,6>, S> const& handle, vtkm::Id expectedLen)
{
  auto portal = handle.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == expectedLen,
                   "topology portal size is incorrect");

  for (vtkm::Id i = 0; i < expectedLen-1; ++i)
  {
    auto v = portal.Get(i);
    vtkm::Vec<int,6> e;
    e[0] = (topology[0] + (i * topology.size()));
    e[1] = (topology[1] + (i * topology.size()));
    e[2] = (topology[2] + (i * topology.size()));
    e[3] = (topology[0] + ((i+1) * topology.size()));
    e[4] = (topology[1] + ((i+1) * topology.size()));
    e[5] = (topology[2] + ((i+1) * topology.size()));
    VTKM_TEST_ASSERT(test_equal(v,e), "incorrect conversion of topology to Cartesian space");
  }

  auto v = portal.Get(expectedLen-1);
  vtkm::Vec<int,6> e;
  e[0] = (topology[0] + ((expectedLen-1) * topology.size()));
  e[1] = (topology[1] + ((expectedLen-1) * topology.size()));
  e[2] = (topology[2] + ((expectedLen-1) * topology.size()));
  e[3] = (topology[0]);
  e[4] = (topology[1]);
  e[5] = (topology[2]);
  VTKM_TEST_ASSERT(test_equal(v,e), "incorrect conversion of topology to Cartesian space");
}

int main(int argc, char** argv)
{
  const std::size_t numPlanes = 8;

  auto coords = vtkm::cont::make_ArrayHandleXGCPointCoordinates(points_rz, numPlanes);
  auto cells = vtkm::cont::make_CellSetToroid(topology, coords);
  VTKM_TEST_ASSERT(cells.GetNumberOfPoints() == coords.GetNumberOfValues(),
                   "number of points don't match between cells and coordinates");

  // Verify the topology by copying it into another array
  {
  vtkm::cont::ArrayHandle< vtkm::Vec<int,6> > output;
  vtkm::worklet::DispatcherMapTopology< CopyTopo > dispatcher;
  dispatcher.Invoke(cells, output);
  verify_topo(output, 8);
  }


  // Verify the reverse topology by copying the number of cells each point is
  // used by it into another array
  {
  vtkm::cont::ArrayHandle< int > output;
  vtkm::worklet::DispatcherMapTopology< CopyReverseCellCount > dispatcher;
  dispatcher.Invoke(cells, output);
  // verify_topo(output, 8);
  }


  return 0;
}
