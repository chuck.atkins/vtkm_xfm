#include "vtkmxfm/vtkmxfm.h"

#include <vtkm/cont/DataSet.h>

#include <vtkm/filter/CellAverage.h>
#include <vtkm/filter/PointAverage.h>

#include <vtkm/cont/testing/Testing.h>

#include "tests/example_data.h"


int main(int argc, char** argv)
{
  const std::size_t numPlanes = 8;

  auto coords = vtkm::cont::make_ArrayHandleXGCPointCoordinates(points_rz, numPlanes);
  auto cells = vtkm::cont::make_CellSetToroid(topology, coords);

  vtkm::cont::DataSet dataset;

  dataset.AddCoordinateSystem(vtkm::cont::CoordinateSystem("coords", coords));
  dataset.AddCellSet(cells);

  // verify that a constant value point field can be accessed
  // simplified example that doesn't add the fields to the dataset
  {
    std::vector<float> values(coords.GetNumberOfValues(), 42.0f);
    auto handle = vtkm::cont::make_ArrayHandle(values);

    vtkm::cont::ArrayHandle<vtkm::Float32> output;

    vtkm::filter::CellAverage avg;
    vtkm::filter::Result result =
      avg.Execute(dataset,
                  vtkm::cont::Field("pfield", vtkm::cont::Field::ASSOC_POINTS, handle),
                  xfmPolicy());

    VTKM_TEST_ASSERT(result.IsDataSetValid(), "filter resulting dataset should be valid");
    VTKM_TEST_ASSERT(result.IsFieldValid(), "filter resulting field should be valid");
  }

  // verify that a constant cell value can be accessed
  // simplified example that doesn't add the fields to the dataset
  {
    std::vector<float> values(cells.GetNumberOfCells(), 42.0f);
    auto handle = vtkm::cont::make_ArrayHandle(values);
    vtkm::cont::ArrayHandle<vtkm::Float32> output;

    vtkm::filter::PointAverage avg;
    vtkm::filter::Result result = avg.Execute(
      dataset,
      vtkm::cont::Field("cfield", vtkm::cont::Field::ASSOC_CELL_SET, cells.GetName(), handle),
      xfmPolicy());

    VTKM_TEST_ASSERT(result.IsDataSetValid(), "filter resulting dataset should be valid");
    VTKM_TEST_ASSERT(result.IsFieldValid(), "filter resulting field should be valid");
  }

  return 0;
}
